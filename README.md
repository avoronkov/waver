# Waver project

## TODO

- [ ] Yaml config instead of json

- [ ] Autoreload on config changes

- [ ] Flanger filter

- [ ] Samples support in config

- [ ] More drum samples!

- [ ] Handle samples as separate instrument (`z`)

- [ ] Semisine wave

- [ ] Improve amplitude parameter
