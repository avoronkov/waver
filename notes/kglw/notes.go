package kglw

import (
	"waver/notes"
)

const half = 1.0594630943593
const quarter = 1.029302236643492

const (
	I    = notes.Fs2
	II   = notes.G2 * quarter
	III  = notes.A2
	IV   = notes.B2
	V    = notes.Cs3
	VI   = notes.D3 * quarter
	VII  = notes.E3
	VIII = notes.Fs3
)
