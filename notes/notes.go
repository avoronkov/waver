package notes

const mp = 1.0594630943593

const (
	A1  = 440.0
	As1 = A1 * mp
	Bb1 = As1
	B1  = Bb1 * mp
	C2  = B1 * mp
	Cs2 = C2 * mp
	Db2 = Cs2
	D2  = Db2 * mp
	Ds2 = D2 * mp
	Eb2 = Ds2
	E2  = Eb2 * mp
	F2  = E2 * mp
	Fs2 = F2 * mp
	Gb2 = Fs2
	G2  = Gb2 * mp
	A2b = G2 * mp

	A2  = 880.0
	As2 = A2 * mp
	Bb2 = As2
	B2  = Bb2 * mp
	C3  = B2 * mp
	Cs3 = C3 * mp
	Db3 = Cs3
	D3  = Db3 * mp
	Ds3 = D3 * mp
	Eb3 = Ds3
	E3  = Eb3 * mp
	F3  = E3 * mp
	Fs3 = F3 * mp
)
